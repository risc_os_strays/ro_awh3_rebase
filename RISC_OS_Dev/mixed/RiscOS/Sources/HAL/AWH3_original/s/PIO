;Copyright (c) 2019, Tristan Mumford
;All rights reserved.
;
;Redistribution and use in source and binary forms, with or without
;modification, are permitted provided that the following conditions are met:
;
;1. Redistributions of source code must retain the above copyright notice, this
;   list of conditions and the following disclaimer.
;2. Redistributions in binary form must reproduce the above copyright notice,
;   this list of conditions and the following disclaimer in the documentation
;   and/or other materials provided with the distribution.
;
;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
;ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
;DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
;ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
;ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;
;The views and conclusions contained in the software and documentation are those
;of the authors and should not be interpreted as representing official policies,
;either expressed or implied, of the RISC OS project.

;//------system includes------
    GET     Hdr:ListOpts
    GET     Hdr:Macros
    GET     Hdr:System
    GET     Hdr:OSEntries

;//------project includes------
    GET     hdr.AllWinnerH3
    GET     hdr.StaticWS
    GET     hdr.Registers    ;might not need this

 [ Debug
    GET     Debug
    IMPORT  HAL_DebugTX
    IMPORT  HAL_DebugRX
    IMPORT  DebugHALPrint
    IMPORT  DebugHALPrintReg
    IMPORT  DebugMemDump
    IMPORT  DebugHALPrintByte
    IMPORT  DebugCallstack
 ]
 
 ;//------Prototypes------
;bool     memAlloc(void);
    EXPORT      memAlloc
;void     initPIO();
    EXPORT      initPIO
;void     setConfig(uint32_t port, uint32_t pad, uint32_t mode);
    EXPORT      setConfig
;uint32_t getConfig(uint32_t port, uint32_t pad);
    EXPORT      getConfig
;void     setDataBit(uint32_t port, uint32_t pad, uint32_t data);
    EXPORT      setDataBit
;uint32_t getDataBit(uint32_t port, uint32_t pad);
    EXPORT      getDataBit
;void     setDrv(uint32_t port, uint32_t pad, uint32_t drv);
    EXPORT      setDrv
;uint32_t getDrv(uint32_t port, uint32_t pad);
    EXPORT      getDrv
;void     setPul(uint32_t port, uint32_t pad, uint32_t mode);
    EXPORT      setPul
;uint32_t getPul(uint32_t port, uint32_t pad);
    EXPORT      getPul
;void     setIntCfg(uint32_t port, uint32_t reg, uint32_t mode);
    EXPORT      setIntCfg
;uint32_t getIntCfg(uint32_t port, uint32_t reg);
    EXPORT      getIntCfg

;bool     write32(uint32_t data, uint32_t offset);
    EXPORT      write32
;uint32_t read32(uint32_t offset);
    EXPORT      read32
    

;bool     write32_L(uint32_t data, uint32_t offset);
    EXPORT      write32_L
;uint32_t read32_L(uint32_t offset);
    EXPORT      read32_L
    
;//------Implementation------
 
    AREA    |ARM$$code|, CODE, READONLY, PIC
;----------------------------    
;bool     memAlloc(void);
memAlloc
    
    MOV     pc, lr
    
;----------------------------    
;void     initPIO();
initPIO

    MOV     pc, lr
    
;----------------------------    
;void     setConfig(uint32_t port, uint32_t pad, uint32_t mode);
setConfig

    MOV     pc, lr
    
;----------------------------        
;uint32_t getConfig(uint32_t port, uint32_t pad);
getConfig

    Push "a3, a4, lr"
    ;a3 = regBit
    ;a4 = cfgreg
;    uint32_t mode;   /// config mode.
;    uint32_t cfgReg; ///CFGn = 0:3
;    uint32_t regBit; /// base bit in CFGn for "pad"
;    uint32_t offset; ///memory offset

;    ///pad needs to be 0:7;

;    regBit = pad;
;    regBit &= 7;
    AND     a3, a2, #7
    

;    regBit = regBit << 2; ///starting bit# in register
    MOV     a3, a3, LSL#2
;//    printf("regBit = %x\n", regBit); ///showing correct value.
;    cfgReg = pad >> 3; ///register. CFGx (0:3)
    MOV     a4, a2, LSR#3
    ;a4 = cfgReg

;    offset = (port * 0x24) + (cfgReg << 2);
;--last instance of cfgReg. Reusing...
    ;MOV     a4, a4, LSL#2
    MUL     a1, a1, #&24 ;Can I do this?
    ADD     a1, a1, a4, LSL#2

;//port not used anymore. a1 = offset
;    //mode = read32(offset);
;    mode = read32(offset);
    BL      read32
    ;a1 = mode

;
;    //change from here down.
;    //shift right by regBit
;    //mask with 7.
;    MOV     a1, a1, LSR
    LSR     a1, a1, a3
;    mode = mode >> regBit;
;    mode &=7; ///make sure only 3 lsb are nonzero.
    AND     a1, a1, 7
;    return mode;
    
    Pull    "a3, a4, lr"
    MOV     pc, lr

;----------------------------            
;void     setDataBit(uint32_t port, uint32_t pad, uint32_t data);
setDataBit
;DON'T USE THIS
;   uint32_t offset;
;   uint32_t tmp;
;
;    a1 = port
;    a2 = pad
;    a3 = data

;    a4 = offset
;     v1 = scratch
    Push    "a4, lr"
;    offset = (port * 0x24) + 0x10;
    ;ehh. whatever
    Push    "a2, a3"
    
    MOV     a3, #&24
    MOV     a2, a1
    MUL     a1, a2, a3
    Pull    "a2, a3"
    ADD     a1, a1, &10
    ;--a1 = offset
;    data &= 1;
    AND     a3, a3, #1
;    pad  &= 0x1F; /// <=31.
    AND     a2, a2, #&1F
; --port ie a1 is no longer used
    MOV     a1, a4 ;put offset in a1 for read.
    BL      read32
    ;--a4 still has offset.
    ;a1 = tmp
    
;    tmp = read32(offset);
    

;    tmp ^= (-data ^ tmp) & (1UL << pad);
;--Don't do this. Do bit shifted BIC, then bit shifted ORR
    BIC     a1, a1, #1, LSL a2
    ORR     a1, a1, a3, LSL a2
    

;    //number ^= (-x ^ number) & (1UL << n);
;    //number = number & ~(1 << n) | (x << n);
;    //bit = (number >> x) & 1
    MOV     a2, a4 ;put offset in a2
    BL      write32
;    write32(tmp, offset);
    Pull    "a4, lr"
    MOV     pc, lr
    
;----------------------------        
;uint32_t getDataBit(uint32_t port, uint32_t pad);
getDataBit
    Push    "a3, lr"
; a1 = port
; a2 = pad
;    bool isPortL = false;

    MOV     a3, a1
    ;a3 = port
    ;a1 is needed for offset
;   uint32_t offset;
;   uint32_t tmp;

;    if(PORTL){
;		//snowflake time
;		port = PORTA; //for offsets.
;		isPortL = true;
;	}
;
;    offset = (port * 0x24) + 0x10;
    Push    "a2"
      MOV     a2, #&24
      MUL    a1, a3, a2
    Pull    "a2"
    
    ADD     a1, a1, #&10
;    pad  &= 0x1F; /// <=31.
    AND     a2, a2, #&1F
;	if(isPortL)
;	  tmp = read32_L(offset);
;	else
;      tmp = read32(offset);
    BL      read32
    
;    tmp = tmp >> pad;
    MOV     a1, a1, LSR a2
;    tmp &=1;
    AND     a1, a1, #&1

;    return tmp;
    Pull    "a3, lr"
    MOV     pc, lr
    
;----------------------------        
;void     setDrv(uint32_t port, uint32_t pad, uint32_t drv);
setDrv
    Push    "a4, v1, lr"
;	uint32_t offset;
;	uint32_t tmp;
;	uint32_t regBit; //register bit.
;	bool isPortL = false;
;	/*
;    Pn_DRV0 n*0x24+0x14 Port n Multi-Driving Register 0 (n from 0 to 6)
;    Pn_DRV1 n*0x24+0x18 Port n Multi-Driving Register 1 (n from 0 to 6)
;     * setConfig uses a better method. TODO: Revise this.
;	 * */
;	if(PORTL){
;		//snowflake time
;		port = PORTA; //for offsets.
;		isPortL = true;
;	}

;	pad &= 0x31;
    AND     a2, a2, #&31
;	drv &= 0x3; //two bits
    AND     a3, a3, #&3
;
;	 // 2i+1 : 2i
;	if(pad < 16)
;port = a4
;offset = a1
    MOV     a4, a1
    CMP     a2, #16
      BLT   %FT10
      B     $FT20
10
    Push    "a2"
      MOV     a2, #&24
      MUL     a1, a4, a2
    Pull    "a2"
    ADD     a1, a1, #&14
    
;		offset = ( port * 0x24) + 0x14; //0:15

    B       %FT30
;	else {
20
    Push    "a2"
      MOV     a2, &24
      MUL     a1, a4, a2
    Pull    "a2"
    ADD     a1, a1, #&14
    
    
;	 offset = ( port * 0x24) + 0x18; //16:21
;	 //mask higher bits of pad.
;	 pad &= 0xF;
    AND     a2, a2, #&F
;   }
30
; a1 = offset
; a2 = pad
; a3 = drv
; a4 = regBit. Was port


;    regBit = pad << 2; //each register is 2 bits per pad.
     MOV    a4, a2, LSH #2
     ;skip...
;    if(isPortL)
;      tmp = read32_L( offset );
;    else
;      tmp = read32(offset); //read reg contents.
     MOV    v1, a1 ;need to preserve offset
     BL      read32
     
     
;    //tmp &= ~( drv << regBit );//modify
;//    tmp &= ~(3 << regBit ) | ( drv << regBit );
;      tmp ^= (-drv ^ tmp) & (3 << regBit);
    BIC     a1, a1, #3, LSH a4
    ORR     a1, a1, a3, LSH a4
    MOV     a2, v1
    BL      write32
;      
;    //tmp &= ~( drv << pad );
;    if(isPortL)
;      write32_L(tmp, offset);
;    else
;      write32(tmp, offset); //write
;
    Pull    "a4, v1, lr"
    MOV     pc, lr
    
 ;----------------------------           
;uint32_t getDrv(uint32_t port, uint32_t pad);
getDrv

    Push "a3, a4, lr"
;    uint32_t offset;
;	uint32_t tmp;
;	uint32_t regBit; //register bit.
;	bool isPortL = false;
;	/*
;	 * Pn_PUL0 n*0x24+0x1C Port n Pull Register 0 (n from 0 to 6)
;    * Pn_PUL1 n*0x24+0x20 Port n Pull Register 1 (n from 0 to 6)
;     * 2 bits per reg. no gaps
;     * setConfig uses a better method. TODO: Revise this.
;	 * */
;
;	if(PORTL){
;		//snowflake time
;		port = PORTA; //for offsets.
;		isPortL = true;
;	}

;	pad &= 0x31;

    AND     a2, a2, #&31
;
;
;	 // 2i+1 : 2i
;	if(pad < 16)
    CMP     a2, #16
    BGE     %FT20

    MOV     a3, a1
    ;port = a3
    ;offset = a1
    
;		offset = ( port * 0x24) + 0x14; //0:15
    MOV     a4, #&24
    MUL     a1, a3, a4
    ADD     a1, a1, #&14
    B       %FT30
20
;	else {
    MOV     a4, #&24
    MUL     a1, a3, a4
    ADD     a1, a1, #&18
    AND     a2, a2, #&F
;	 offset = ( port * 0x24) + 0x18; //16:21
;	 //mask higher bits of pad.
;	 pad &= 0xF;
;   }
30
    ;a4 = regbit
    ;below we have tmp, regbit, pad, offset
;    a1 = offset
;    a2 = pad
;    a3 = port
;    a4 = regbit
    
;    regBit = pad << 2; //each register is 2 bits per pad.
    MOV     a4, a2, LSL#2
    ;a3 = copy of offset
    MOV     a3, a1
    BL      read32
    ;a1 = tmp
;    if(isPortL)
;      tmp = read32_L(offset);
;    else
;      tmp = read32(offset); //read reg contents.
    MOV     a1, a1, LSR a4
;    tmp = tmp >> regBit;
;    tmp &= 0x3; //clear up high bits.
    AND     a1, a1, #&3

;	return( tmp );
    Pull    "a3, a4, lr"
    MOV     pc, lr
    
 ;----------------------------           
 ;void     setPul(uint32_t port, uint32_t pad, uint32_t mode);
setPul
    Push "a4, v1, v2, lr"

;	uint32_t offset;
;	uint32_t tmp;
;	uint32_t regBit; //register bit.
;	bool isPortL = false;
;	/*
;	 * Pn_PUL0 n*0x24+0x1C Port n Pull Register 0 (n from 0 to 6)
;    * Pn_PUL1 n*0x24+0x20 Port n Pull Register 1 (n from 0 to 6)
;     * 2 bits per reg. no gaps
;     * setConfig uses a better method. TODO: Revise this.
;	 * */
;	if(PORTL){
;		//snowflake time
;		port = PORTA; //for offsets.
;		isPortL = true;
;	}
;   a1 = port
;   a2 = pad
;   a3 = mode
;   v1 = scratch
;	pad &= 0x31;
    AND     a2, a2, #&31
;	mode &= 0x3; //two bits
    AND     a3, a3, #&3
;
;	 // 2i+1 : 2i
;	if(pad < 16)
;move port to a4
    MOV     a4, a1
    MOV     v1, #&24
    MUL     a1, a3, v1 
    CMP     a2, #16
    ADDLT   a1, a1, #&1C
    ADDGEQ  a1, a1, #&20
    ANDGEQ  a2, a2, #&F
    ;dorp through
10
; a1 = offset
; a2 = pad
; a3 = mode
; a4 = port
;		offset = ( port * 0x24) + 0x1C; //0:15
;	else {
20
;	 offset = ( port * 0x24) + 0x20; //16:21
;	 //mask higher bits of pad.
;	 pad &= 0xF;
;    }
30
    ;v1 = regBit
    ;v2 = tmp
    
;   regBit = pad << 2; //each register is 2 bits per pad.
    MOV     v1, v1, LSL#2
    MOV     v2, a1 ; tuck away offset for now. read32 will clobber it
    BL      read32

;    if(isPortL)
;      tmp = read32_L(offset);
;    else
;      tmp = read32(offset); //read reg contents.
;    //tmp &= ~( 3 << regBit ) | ( mode << regBit );
;    tmp ^= (-mode ^ tmp) & (3 << regBit); //3 = 0b11. ie 2 bits
    BIC     a1, a1, 3, LSL v1
    ORR     a1, a1, a3, LSL v1
    ;put offset somewehere useful
    MOV     a2, v2
    BL      write32
;
;    //tmp &= ~( mode << regBit );//modify
;    //tmp &= ~( drv << pad );
;    if(isPortL)
;      write32_L(tmp, offset);
;    else
;      write32(tmp, offset); //write

    Pull    "a4, v1, v2, lr"
    MOV     pc, lr
    
 ;----------------------------           
;uint32_t getPul(uint32_t port, uint32_t pad);
getPul

    MOV     pc, lr
    
 ;----------------------------           
;void     setIntCfg(uint32_t port, uint32_t reg, uint32_t mode);
setIntCfg

    MOV     pc, lr
    
 ;----------------------------               
;uint32_t getIntCfg(uint32_t port, uint32_t reg);
getIntCfg

    MOV     pc, lr
    
 ;----------------------------               
;bool     write32(uint32_t data, uint32_t offset);
write32
    Push "a3"
    
    LDR     a3, =PIO_BaseAddr
    STR     a1, [a3, a2]
    
    Pull "a3"
    MOV     pc, lr
    
 ;----------------------------               
;uint32_t read32(uint32_t offset);
read32
    Push    "a2"
    LDR     a2, =PIO_BaseAddr
    LDR     a1, [a2, a1]
    Pull    "a2"
    MOV     pc, lr


;Below shouldn't be needed.    
 ;----------------------------               
;bool     write32_L(uint32_t data, uint32_t offset);
write32_L
    Push "a3"
    
    LDR     a3, =R_PIO_BaseAddr
    STR     a1, [a3, a2]
    
    Pull "a3"

    MOV     pc, lr
    
 ;----------------------------               
;uint32_t read32_L(uint32_t offset);
read32_L
    Push    "a2"
    LDR     a2, =R_PIO_BaseAddr
    LDR     a1, [a2, a1]
    Pull    "a2"
    MOV     pc, lr
    

    END
