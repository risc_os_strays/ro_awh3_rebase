#!/bin/bash
cwd="$(pwd)"
#use this to copy files from working tree to clean tree for uploading.
#Relevent directories have to be cleaned with build tools first to avoid binary uploads and altered makefiles.
#HAL
rsync -ruv "$cwd"/RISC_OS_Dev/mixed/RiscOS/Sources/HAL/AWH3/ "$1"/RISC_OS_Dev/mixed/RiscOS/Sources/HAL/AWH3
#Components
#rsync -uv "$cwd"/RISC_OS_Dev/BuildSys/Components/ROOL/AWH3 "$1"/RISC_OS_Dev/BuildSys/Components/ROOL/AWH3
rsync -uv "$cwd"/RISC_OS_Dev/apache/RiscOS/BuildSys/Components/ROOL/AWH3 "$1"/RISC_OS_Dev/apache/RiscOS/BuildSys/Components/ROOL/AWH3
#Env
#rsync -uv "$cwd"/RISC_OS_Dev/Env/ROOL/AWH3* "$1"/RISC_OS_Dev/Env/ROOL/
rsync -uv "$cwd"/RISC_OS_Dev/apache/RiscOS/Env/ROOL/AWH3* "$1"/RISC_OS_Dev/apache/RiscOS/Env/ROOL/
#ModuleDB
#rsync -uv "$cwd"/RISC_OS_Dev/BuildSys/ModuleDB "$1"/RISC_OS_Dev/BuildSys/ModuleDB
rsync -uv "$cwd"/RISC_OS_Dev/apache/RiscOS/BuildSys/ModuleDB "$1"/RISC_OS_Dev/apache/RiscOS/BuildSys/ModuleDB

#Products
rsync -ruv "$cwd"/RISC_OS_Dev/Products/ "$1"/RISC_OS_Dev/Products

#Video driver
#-check this later-
mkdir -p "$1"/RISC_OS_Dev/mixed/RiscOS/Sources/Video/HWSupport/
rsync -ruv "$cwd"/RISC_OS_Dev/mixed/RiscOS/Sources/Video/HWSupport/NewVideo "$1"/RISC_OS_Dev/mixed/RiscOS/Sources/Video/HWSupport/

#Kernel options
rsync -uv "$cwd"/RISC_OS_Dev/apache/RiscOS/Sources/Kernel/hdr/Options* "$1"/RISC_OS_Dev/apache/RiscOS/Sources/Kernel/hdr/

#self
rsync -uv "$cwd"/sync.sh "$1"
